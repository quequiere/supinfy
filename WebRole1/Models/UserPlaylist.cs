﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.Models
{
    public class UserPlaylist
    {
        public int Id { get; set; }
        public string trackId { get; set; }
        public virtual User user { get; set; }

        public UserPlaylist(string trackId) : this()
        {
            this.trackId = trackId;
        }

        public UserPlaylist() { }
    }
}