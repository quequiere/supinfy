﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebRole1.Models
{
    public class User
    {
        //Auto generated id from db
        public int Id { get; set; }

        public string Username { get; set; }

        public string Mail { get; set; }

        public virtual List<UserHistory> UserHistories { get; set; }
        public virtual List<UserPlaylist> UserPlaylist { get; set; }

        public User(string username , string mail) : this()
        {
            this.Username = username;
            this.Mail = mail;
        }

        //Needed to search user in db
        public User()
        {
            this.UserHistories = new List<UserHistory>();
            this.UserPlaylist = new List<UserPlaylist>();
        }
    }
}