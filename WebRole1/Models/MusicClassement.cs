﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.Models
{
    public class MusicClassement
    {
        public string titre;
        public string artiste;
        public int listenNumber;

        public MusicClassement(string titre, string artiste, int listenNumber)
        {
            this.titre = titre;
            this.artiste = artiste;
            this.listenNumber = listenNumber;
        }
    }
}