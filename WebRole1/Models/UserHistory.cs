﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.Models
{
    public class UserHistory
    {
        public int Id { get; set; }
        public int userId { get; set; }
        public string trackId { get; set; }
        public long timestamp { get; set; }

        public virtual User user { get; set; }

        public UserHistory(string trackId, long timestamp) : this()
        {
            this.trackId = trackId;
            this.timestamp = timestamp;
        }

        public UserHistory() { }
    }
}