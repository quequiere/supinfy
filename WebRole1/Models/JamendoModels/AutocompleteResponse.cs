﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.Models.JamendoModels
{
    public class AutocompleteResponse
    {
        public JamendoAutoCompleteResults results;
    }

    public class JamendoAutoCompleteResults
    {
        public List<string> tracks;
    }
}