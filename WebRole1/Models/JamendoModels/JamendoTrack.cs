﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebRole1.Models.JamendoModels
{
    public class JamendoTrack
    {
        public string id;
        public string name;

        public int duration;

        public string artist_name;
        public string artist_id;

        public string album_id;
        public string album_name;

        public string album_image;

        public string audio;
        public string audiodownload;
    }

    public class JamendoTracksResponse
    {
        public List<JamendoTrack> results;
    }
}