﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using WebRole1.Component;
using WebRole1.Component.DataStorage;
using WebRole1.Component.Music;
using WebRole1.Models;
using WebRole1.Models.JamendoModels;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace WebRole1.Controllers
{
    public class MusicController : Controller
    {
        private IJamendoComponent jamendoComponent;
        private IUserComponent userComponent;
        private IMusicDatabaseComponent musicDatabaseComponent;
        private IMusicPlaylistComponent musicPlaylistComponent;

        public MusicController (IJamendoComponent jamendoComponent, IUserComponent userComponent, IMusicDatabaseComponent musicDatabaseComponent, IMusicPlaylistComponent musicPlaylistComponent)
        {
            this.jamendoComponent = jamendoComponent;
            this.userComponent = userComponent;
            this.musicDatabaseComponent = musicDatabaseComponent;
            this.musicPlaylistComponent = musicPlaylistComponent;
        }

        // GET: Music
        public ActionResult SearchTitle(string trackname = null)
        {
            if (trackname != null)
            {
                ViewBag.searchedTrack = trackname;
                ViewBag.foundedTracks = jamendoComponent.FindTrack(trackname);
            }

            return View("SearchTitleView");
        }

        public ActionResult DisplayTitle(string trackID = null)
        {
            string username = User.Identity.GetUserName();
            User user;

            if (!this.userComponent.getUserByName(username, out user))
            {
                ViewBag.error ="Sorry you must be identified to reach this feature!";
                return View("Play");
            }

            if (trackID == null)
            {
                ViewBag.error = "Sorry buy you need to provide a track to play";
            }

            JamendoTrack track = jamendoComponent.GetTrackById(trackID);

            if (track == null)
            {
                ViewBag.error = "Oups ! We can't find this track !";
                return View("Play");
            }

            ViewBag.currentTrack = track;

            //Added music to user history
            this.musicDatabaseComponent.addListenMusic(user, track.id);

            //Display if the music was already added to playlist
            ViewBag.tittleAddedPlaylist = musicPlaylistComponent.getPlaylist(user).Any( p => p.id == track.id);


            ViewBag.foundedTracks = musicPlaylistComponent.getPlaylist(user).DistinctBy(p => p.id).ToList();

            return View("Play");
        }

        [System.Web.Mvc.HttpGet]
        public JsonResult completeTitle(string title)
        {
            return Json(jamendoComponent.AutoCompleteTittle(title),JsonRequestBehavior.AllowGet);
        }


    }
}