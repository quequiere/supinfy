﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebRole1.Component.DataStorage;
using WebRole1.Component.Music;
using WebRole1.Models;
using WebRole1.Models.JamendoModels;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace WebRole1.Controllers
{
    [System.Web.Mvc.RoutePrefix("api")]
    public class MusicPlaylistController : Controller
    {
        private IMusicPlaylistComponent playlistComponent;
        private IUserComponent userComponent;

        public MusicPlaylistController(IMusicPlaylistComponent playlistComponent, IUserComponent userComponent)
        {
            this.playlistComponent = playlistComponent;
            this.userComponent = userComponent;
        }


        [System.Web.Mvc.HttpGet]
        [System.Web.Mvc.Route("playlist")]
        public ActionResult viewPlaylist()
        {
            string userName = User.Identity.GetUserName();
            List<JamendoTrack> result;
            if (userComponent.getUserByName(userName, out User user))
            {
                result =  playlistComponent.getPlaylist(user);
            }
            else
            {
                result =  new List<JamendoTrack>();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [System.Web.Mvc.HttpPut]
        [System.Web.Mvc.Route("playlist")]
        public ActionResult addPlaylist([FromBody]string trackid)
        {
            string userName = User.Identity.GetUserName();
            if (trackid!=null && userComponent.getUserByName(userName, out User user))
            {
                if (playlistComponent.getPlaylist(user).All(p => p.id != trackid))
                {
                    playlistComponent.addToPlaylist(user, trackid);
                    return Json(new { success = true });
                }
            }
            return Json(new { success = false });

        }

        [System.Web.Mvc.HttpDelete]
        [System.Web.Mvc.Route("playlist")]
        public ActionResult deteleInPlaylist(string trackid)
        {
            string userName = User.Identity.GetUserName();
            if (trackid != null && userComponent.getUserByName(userName, out User user))
            {
                playlistComponent.remoteFromPlaylist(user, trackid);
                return Json(new { success = true });
            }
            return Json(new { success = false });
        }
    }
}