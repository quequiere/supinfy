﻿using System.Web.Mvc;
using WebRole1.Component.Music;

namespace WebRole1.Controllers
{
    public class MusicTopController : Controller
    {
        private IMusicClassementComponent musicClassementComponent;

        public MusicTopController(IMusicClassementComponent musicClassementComponent)
        {
            this.musicClassementComponent = musicClassementComponent;
        }

        public ActionResult DisplayTopTen()
        {
            
            ViewBag.TopTen = musicClassementComponent.DisplayTopTenMusic();

            return View("TopTen");
        }
    }
}