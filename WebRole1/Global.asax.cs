﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using WebRole1.Component;
using WebRole1.Component.DataStorage;
using WebRole1.Component.Music;
using WebRole1.Component.WebServices;
using WebRole1.Models;

namespace WebRole1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //Register here all component as instance per request to don't overlead app.
            builder.RegisterType<JamendoComponent>().As<IJamendoComponent>().InstancePerRequest();
            builder.RegisterType<DatabaseComponent>().As<IDatabaseComponent>().InstancePerRequest();
            builder.RegisterType<UserComponent>().As<IUserComponent>().InstancePerRequest();
            builder.RegisterType<MusicClassementComponent>().As<IMusicClassementComponent>().InstancePerRequest();
            builder.RegisterType<WebClientComponent>().As<IWebClientComponent>().InstancePerRequest();
            builder.RegisterType<MusicDatabaseComponent>().As<IMusicDatabaseComponent>().InstancePerRequest();
            builder.RegisterType<MusicPlaylistComponent>().As<IMusicPlaylistComponent>().InstancePerRequest();



            //register database context
            builder.RegisterType<SupinfyDBContext>();

            var container = builder.Build();
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(resolver);


            var userComponent = resolver.GetService<IUserComponent>();

            if (!userComponent.getUserByName("test", out User u))
            {
                userComponent.CreateUser("test", "test@test.fr");
            }

        }
    }
}
