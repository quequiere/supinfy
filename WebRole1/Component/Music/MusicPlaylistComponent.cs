﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebRole1.Component.DataStorage;
using WebRole1.Models;
using WebRole1.Models.JamendoModels;

namespace WebRole1.Component.Music
{
    public class MusicPlaylistComponent: IMusicPlaylistComponent
    {
        private IMusicDatabaseComponent musicDatabaseComponent;
        private IJamendoComponent jamendoComponent;

        public MusicPlaylistComponent(IMusicDatabaseComponent musicDatabaseComponent, IJamendoComponent jamendo)
        {
            this.musicDatabaseComponent = musicDatabaseComponent;
            this.jamendoComponent = jamendo;
        }

        public List<JamendoTrack> getPlaylist(User user)
        {
           return musicDatabaseComponent.getUserPlaylist(user).Select(t =>
            {
                return jamendoComponent.GetTrackById(t);
            }).ToList();
        }

        public void addToPlaylist(User user, string trackId)
        {
            musicDatabaseComponent.addUserPlaylistTrack(user, trackId);
        }

        public void remoteFromPlaylist(User user, string trackId)
        {
            musicDatabaseComponent.removeUserPlaylistTrack(user, trackId);
        }
    }

    public interface IMusicPlaylistComponent
    {
        List<JamendoTrack> getPlaylist(User user);
        void addToPlaylist(User user, string trackId);
        void remoteFromPlaylist(User user, string trackId);
    }
}