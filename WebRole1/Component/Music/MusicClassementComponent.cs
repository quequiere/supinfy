﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebRole1.Component.DataStorage;
using WebRole1.Models;
using WebRole1.Models.JamendoModels;

namespace WebRole1.Component.Music
{
    public class MusicClassementComponent : IMusicClassementComponent
    {
        private IMusicDatabaseComponent musicDatabaseComponent;
        private IJamendoComponent jamendoComponent;

        public MusicClassementComponent(IMusicDatabaseComponent musicDatabaseComponent, IJamendoComponent jamendoComponent)
        {
            this.musicDatabaseComponent = musicDatabaseComponent;
            this.jamendoComponent = jamendoComponent;
        }


        public List<MusicClassement> DisplayTopTenMusic()
        {
            return musicDatabaseComponent.getMusicHistoryGrouped(10).Select(x =>
            {
                JamendoTrack track = jamendoComponent.GetTrackById(x.Key);
                return new MusicClassement(track?.name ?? "Track name not loaded", track?.artist_name ?? "Artist name not found", x.Value);

            }).ToList();
        }

    }

    public interface IMusicClassementComponent
    {
        List<MusicClassement> DisplayTopTenMusic();

    }
}