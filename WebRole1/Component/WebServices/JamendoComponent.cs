﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Web;
using WebRole1.Component.WebServices;
using WebRole1.Models.JamendoModels;

namespace WebRole1.Component
{
    public class JamendoComponent : IJamendoComponent
    {
        private IWebClientComponent webClientComponent;

        public JamendoComponent(IWebClientComponent webClientComponent)
        {
            this.webClientComponent = webClientComponent;
        }


        public List<string> AutoCompleteTittle(string title)
        {
            if(title != null)
            {
                var arguments = new Dictionary<string, string>() { { "entity", "tracks" }, { "prefix", title } };
                if (webClientComponent.GetData(HttpMethod.Get, "autocomplete/", arguments, out AutocompleteResponse jamendoResponse))
                {
                    return jamendoResponse.results.tracks;
                }
            }

            return new List<string>();
        }

        public List<JamendoTrack> FindTrack(string title)
        {
            var arguments = new Dictionary<string, string>() { { "namesearch", title } };

            if (webClientComponent.GetData(HttpMethod.Get, "tracks/", arguments, out JamendoTracksResponse jamendoResponse))
            {
                return jamendoResponse.results;
            }
            return new List<JamendoTrack>();
        }

        public JamendoTrack GetTrackById(string id)
        {
            var arguments = new Dictionary<string, string>() { { "id", id } };
            if (webClientComponent.GetData(HttpMethod.Get, "tracks/", arguments, out JamendoTracksResponse jamendoResponse))
            {
                return jamendoResponse.results.FirstOrDefault();
            }

            return null;
        }
    }

    public interface IJamendoComponent
    {

        List<string> AutoCompleteTittle(string title);
        List<JamendoTrack>FindTrack(string title);
        JamendoTrack GetTrackById(string id);

    }
}