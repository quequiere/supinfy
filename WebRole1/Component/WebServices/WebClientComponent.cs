﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace WebRole1.Component.WebServices
{
    public class WebClientComponent : IWebClientComponent
    {
        private readonly Uri baseUri = new Uri($"https://api.jamendo.com/v3.0/");
        private readonly string clientId = "f417759e";

        private readonly HttpClient httpClient = new HttpClient();
        

        public bool GetData<T>(HttpMethod method, string relativePath,Dictionary<string,string> urlParameters, out T returnedObject)
        {
            returnedObject = default(T);
            string json = null;


            var builder = new UriBuilder($"{baseUri}{relativePath}");

            var query = HttpUtility.ParseQueryString(builder.Query);
            query["client_id"] = "f417759e";
            query["format"] = "jsonpretty";

            if (urlParameters != null)
            {
                foreach (var key in urlParameters.Keys)
                {
                    query[key] = urlParameters[key];
                }
            }
               

            builder.Query = query.ToString();
            var uriRequest = new Uri(builder.ToString());

            Debug.WriteLine($"Request ==> {uriRequest.ToString()}");

            var request = new HttpRequestMessage(method, uriRequest);

            try
            {
                var response = this.httpClient.SendAsync(request).Result;

                if (response.IsSuccessStatusCode)
                {
                    json = response.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrEmpty(json))
                    {
                        returnedObject = JsonConvert.DeserializeObject<T>(json);
                        return true;
                    }
                }
                else
                {
                    json = response.Content.ReadAsStringAsync().Result;
                    Debug.WriteLine($"Error {response.StatusCode} from server for url {uriRequest.ToString()} and details: {json}");
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception while request url {uriRequest.ToString()}  {e.Message} and content {json}");
            }

            return false;
        }
    }

    public interface IWebClientComponent
    {
         bool GetData<T>(HttpMethod method, string relativePath, Dictionary<string, string> urlParameters, out T returnedObject);
    }
}