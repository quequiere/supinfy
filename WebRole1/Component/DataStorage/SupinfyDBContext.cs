﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebRole1.Models;

namespace WebRole1.Component.DataStorage
{
    //Register here all objects you need to save into dabase
    public class SupinfyDBContext : DbContext
    {
        public SupinfyDBContext() : base("SupinfyDBContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        //Add here all table needed
        public DbSet<User> uUsers { get; set; }
        public DbSet<UserHistory> UserHistories { get; set; }
        public DbSet<UserPlaylist> UserPlaylists { get; set; }
    }
}