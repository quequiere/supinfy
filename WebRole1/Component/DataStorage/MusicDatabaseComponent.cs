﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebRole1.Models;

namespace WebRole1.Component.DataStorage
{
    public class MusicDatabaseComponent : IMusicDatabaseComponent
    {
        private IDatabaseComponent databaseComponent;

        public MusicDatabaseComponent(IDatabaseComponent databaseComponent)
        {
            this.databaseComponent = databaseComponent;
        }

        /// <summary>
        /// Returns all tracks listened, and how many times are they listened
        /// Already ordered desc
        /// take only x (maxResult) first results, or set maxresult to null to get all results
        /// </summary>
        /// <returns>exemple of a dictionary row : [trackID, 456]</returns>
        public Dictionary<string, int> getMusicHistoryGrouped(int? maxResult)
        {
            var query = databaseComponent.GetHistoriesDatabase()
                .GroupBy
                (
                    h => h.trackId,
                    (key, values) => new { trackid = key, listened = values.Count() }
                )
                .OrderByDescending(t => t.listened);

            if (maxResult.HasValue)
            {
                query.Take(maxResult.Value);
            }

            return query.ToDictionary(s => s.trackid, x => x.listened);
        }


        public List<string> getUserPlaylist(User user)
        {
            return user.UserPlaylist.Select(p => p.trackId).ToList();
        }

        public void addUserPlaylistTrack(User user, string trackId)
        {
            user.UserPlaylist.Add(new UserPlaylist(trackId));
            this.databaseComponent.saveChange();
        }

        public void removeUserPlaylistTrack(User user, string trackId)
        {
            user.UserPlaylist.RemoveAll(p => p.trackId == trackId);
            this.databaseComponent.saveChange();
        }


        public void addListenMusic(User user, string trackId)
        {
            user.UserHistories.Add(new UserHistory(trackId, DateTimeOffset.Now.ToUnixTimeMilliseconds() / 1000));
            this.databaseComponent.saveChange();
        }
    }

    public interface IMusicDatabaseComponent
    {
        Dictionary<string, int> getMusicHistoryGrouped(int? maxResult);
        List<string> getUserPlaylist(User user);
        void addUserPlaylistTrack(User user, string trackId);
        void removeUserPlaylistTrack(User user, string trackId);
        void addListenMusic(User u, string trackId);
    }
}