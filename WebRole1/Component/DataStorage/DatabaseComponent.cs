﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebRole1.Models;

namespace WebRole1.Component.DataStorage
{
    public class DatabaseComponent : IDatabaseComponent
    {
        private SupinfyDBContext databaseContex;

        public DatabaseComponent(SupinfyDBContext databaseContex)
        {
            this.databaseContex = databaseContex;
        }


        public DbSet<User> getUserDatabase()
        {
            return this.databaseContex.uUsers;
        }

        public DbSet<UserHistory> GetHistoriesDatabase()
        {
            return this.databaseContex.UserHistories;
        }

        public DbSet<UserPlaylist> GetUserPlaylistDatabase()
        {
           return this.databaseContex.UserPlaylists;
        }

        public void saveChange()
        {
            this.databaseContex.SaveChanges();
        }
    }


    //Create here all function neededFrom database
    public interface IDatabaseComponent
    {
        DbSet<User> getUserDatabase();
        DbSet<UserHistory> GetHistoriesDatabase();
        DbSet<UserPlaylist> GetUserPlaylistDatabase();

        void saveChange();

    }


  
}