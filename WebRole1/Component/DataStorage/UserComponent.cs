﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebRole1.Models;

namespace WebRole1.Component.DataStorage
{
    public class UserComponent : IUserComponent
    {

        private IDatabaseComponent databaseComponent;

        public UserComponent(IDatabaseComponent databaseComponent)
        {
            this.databaseComponent = databaseComponent;
        }

        public bool getUserByName(string name, out User user)
        {
            user = this.databaseComponent.getUserDatabase().FirstOrDefault(u => u.Username == name);
            return user != null;
        }

        public User CreateUser(string name, string mail)
        {
            User generatedUser = this.databaseComponent.getUserDatabase().Add(new User(name, mail));
            this.databaseComponent.saveChange();
            return generatedUser;
        }
    }

    public interface IUserComponent
    {
        bool getUserByName(string name, out User user);
        User CreateUser(string name, string mail);
    }
}